var gulp = require('gulp');

var livereload     = require('gulp-livereload');
var connect        = require('gulp-connect');
var inlineCss      = require('gulp-inline-css');


gulp.task('connect', function() {
    connect.server({
        livereload: true
    });
});

gulp.task('html', function() {
    gulp.src('./*.html')
        .pipe(livereload());
});

gulp.task('css', function() {
    gulp.src('./*.css')
        .pipe(livereload());
});



var filesToWatch = [
    './*.css',
    './*.html'
]

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(filesToWatch, ['css', 'html']);
});

gulp.task('inline', function() {
    return gulp.src('./*.html')
        .pipe(inlineCss({
            removeStyleTags: true,
            preserveMediaQueries: true,
            applyTableAttributes: true
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('default', ['html', 'css', 'watch', 'connect']);

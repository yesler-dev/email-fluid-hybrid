# Fluid/Hybrid Email example

Based on [example](https://webdesign.tutsplus.com/tutorials/creating-a-future-proof-responsive-email-without-media-queries--cms-23919) by Nicole Merlin.

## What is fluid hybrid design?
Fluid design has been around for a while. This technique uses content blocks that have width: 100%, and max-width: 600px; or whatever width the creator desires. In this way, larger screens will show a centered, 600px wide column of content, while tablets and phones will show the content filling the screen. The issue here is that Outlook (various versions) doesn't support max-width. In Outlook, emails built using this technique will be blown out to the full width of the viewing pane.

To overcome this limitation, "ghost tables" can be used to constrain the email in Outlook. Ghost tables are fixed-width tables built around the fluid tables, but wrapped in MSO conditional tags. The conditional tags make sure that the ghost tables are visible only to Outlook desktop clients. This is the hybrid aspect of this coding technique. [Geoff Phillips](https://www.emailonacid.com/blog/article/email-development/a-fluid-hybrid-design-primer)

### Using
1. [Gulp](https://gulpjs.com/)
2. [Inline-css](https://github.com/jonkemp/gulp-inline-css)

### Inline CSS
Inline CSS lets you write a seperate stylesheet (like a normal workflow!) and then has all the styles inlined into the html. The `<link rel="stylesheet" type="text/css" href="./style.css" />` will be removed. Styles that require media queries will still be placed in the head of the document (for now...).

### Fluid/Hybrid
The `style.css` is commented to help explain what and why certain styles exist. For a more in-depth explanation please read [Nicole Merlin's](https://webdesign.tutsplus.com/tutorials/creating-a-future-proof-responsive-email-without-media-queries--cms-23919) amazing write-up.




